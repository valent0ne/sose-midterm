package youtube.video.streaming.server;

import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.xml.ws.Endpoint;

public class YouTubeVideoStreamingServer {

	public static void main(String[] args) {

		// pool instantiation
		ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);
		Endpoint endpoint = Endpoint.create(new YouTubeVideoStreamingImpl());
		endpoint.setExecutor(threadPoolExecutor);
		endpoint.publish("http://192.168.0.3:20000/youtubevideostreaming");

		// creates and executes a periodic action
		final ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

		scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
			public void run() {

				int queued = threadPoolExecutor.getQueue().size();
				int active = threadPoolExecutor.getActiveCount();
				int notCompleted = queued + active; // approximate value

				System.out.println("at time " + new Timestamp((new Date()).getTime()) + " queued tasks: " + queued);
				System.out.println("at time " + new Timestamp((new Date()).getTime()) + " active tasks: " + active);
				System.out.println("at time " + new Timestamp((new Date()).getTime()) + " notCompleted tasks: " + notCompleted);
				/*
				 * if (active == 5 && queued == 2) {
				 * 		threadPoolExecutor.setMaximumPoolSize(6);
				 * 		threadPoolExecutor.setCorePoolSize(6);
				 * 		System.out.println("... setMaximumPoolSize and setCorePoolSize: 6");
				 * }
				 */
				System.out.println("-----------------------------------------------------");
			}
		}, 0, 1, TimeUnit.SECONDS);
	}

}
