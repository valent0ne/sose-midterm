package youtube.video.streaming.client;

public class StreamLongVideoClient {

   public static void main(String[] args) {

      for (int i = 0; i < 10; i++) {
    	  StreamLongVideoInvokerThread streamLongVideoInvokerThread = 
               new StreamLongVideoInvokerThread(i+1);
         try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
         streamLongVideoInvokerThread.start();
      }
   }

}
