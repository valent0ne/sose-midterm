package youtube.video.streaming.client.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.activation.DataHandler;

public class YoutubeVideoStreamingUtils {

	public static void saveVideo(final DataHandler dataHandler, final File filePath) throws IOException {
		filePath.getParentFile().mkdirs();
		FileOutputStream fileOutputStream = new FileOutputStream(filePath);
		dataHandler.writeTo(fileOutputStream);
		fileOutputStream.flush();
	}

	// -----------------------------------------------------------------------
	/**
	 * <p>
	 * {@code YoutubeStreamingVideoUtils} instances should NOT be constructed in
	 * standard programming. Instead, the class should be used statically.
	 * </p>
	 *
	 * <p>
	 * This constructor is public to permit tools that require a JavaBean instance
	 * to operate.
	 * </p>
	 */
	public YoutubeVideoStreamingUtils() {
		super();
	}

}
