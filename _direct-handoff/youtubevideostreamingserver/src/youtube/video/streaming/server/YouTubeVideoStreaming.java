package youtube.video.streaming.server;

import javax.activation.DataHandler;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

/**
 * We use RPC SOAPBinding to implies that SOAP body contains an element with the
 * name of the method or operation being invoked. This element in turn contains
 * an element for each parameter of that method/operation.
 */
@WebService
@SOAPBinding(style = Style.RPC)
public interface YouTubeVideoStreaming {
	/**
	 * Simulates the streaming of a YouTube short video
	 * 
	 * @param clientID the id of the client
	 * @return a DataHandler to manage a simple stream
	 */
	@WebMethod
	DataHandler streamShortVideo(int clientID);

	/**
	 * Simulates the streaming of a YouTube long video
	 * 
	 * @param clientID the id of the client
	 * @return a DataHandler to manage a simple stream
	 */
	@WebMethod
	DataHandler streamLongVideo(int clientID);
	
}
