package youtube.video.streaming.server;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.jws.WebService;
import javax.xml.ws.soap.MTOM;

/**
 * MTOM is the W3C Message Transmission Optimization Mechanism, a method of
 * efficiently sending binary data to and from Web services.
 */
@MTOM
@WebService(endpointInterface = "youtube.video.streaming.server.YouTubeVideoStreaming")
public class YouTubeVideoStreamingImpl implements YouTubeVideoStreaming {
	private final static String VIDEO_PATH = "resources" + File.separatorChar;
	private final static String VIDEO_EXTENSION = ".avi";

	@Override
	public DataHandler streamShortVideo(int clientID) {
		System.out.println("START streamShortVideo request management at time " + new Timestamp((new Date()).getTime())
				+ " for " + "clientID " + clientID);
		
		FileDataSource dataSource = new FileDataSource(VIDEO_PATH + "50mb" + VIDEO_EXTENSION);
		return new DataHandler(dataSource);
	}

	@Override
	public DataHandler streamLongVideo(int clientID) {
		System.out.println("START streamLongVideo request management at time " + new Timestamp((new Date()).getTime())
				+ " for " + "clientID " + clientID);
		
		FileDataSource dataSource = new FileDataSource(VIDEO_PATH + "300mb" + VIDEO_EXTENSION);
		return new DataHandler(dataSource);
	}
}
