package youtube.video.streaming.server;

import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.*;

import javax.xml.ws.Endpoint;

public class YouTubeVideoStreamingServer {

	public static void main(String[] args) {

		// pool instantiation
		// ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);
	ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5,
																	5,
																	0L,
																	TimeUnit.MILLISECONDS,
																	new LinkedBlockingQueue<>());
		Endpoint endpoint = Endpoint.create(new YouTubeVideoStreamingImpl());
		endpoint.setExecutor(threadPoolExecutor);
		endpoint.publish("http://127.0.0.1:20000/youtubevideostreaming");

		// creates and executes a periodic action
		final ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

		scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
			public void run() {

				int queued = threadPoolExecutor.getQueue().size();
				int active = threadPoolExecutor.getActiveCount();
				int notCompleted = queued + active; // approximate value

				System.out.println("at time " + new Timestamp((new Date()).getTime()) + " queued tasks: " + queued);
				System.out.println("at time " + new Timestamp((new Date()).getTime()) + " active tasks: " + active);
				System.out.println("at time " + new Timestamp((new Date()).getTime()) + " notCompleted tasks: " + notCompleted);

				if (queued > 5) {
					int newSize= threadPoolExecutor.getPoolSize()+1;
					threadPoolExecutor.setMaximumPoolSize(newSize);
					threadPoolExecutor.setCorePoolSize(newSize);
					System.out.println("increasing poolSize to: "+ newSize);
				}

				if (queued <= 5 && threadPoolExecutor.getPoolSize() > 5){
					int newSize = threadPoolExecutor.getPoolSize()-1;
					threadPoolExecutor.setCorePoolSize(newSize);
					threadPoolExecutor.setMaximumPoolSize(newSize);
					System.out.println("decreasing poolSize to: "+ newSize);
				}

				System.out.println("-----------------------------------------------------");
			}
		}, 0, 500, TimeUnit.MILLISECONDS);
	}

}
