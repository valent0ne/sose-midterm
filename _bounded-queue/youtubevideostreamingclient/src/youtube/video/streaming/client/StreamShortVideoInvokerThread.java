package youtube.video.streaming.client;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import javax.activation.DataHandler;

import youtube.video.streaming.client.utils.YoutubeVideoStreamingUtils;

public class StreamShortVideoInvokerThread extends Thread {
	// FIX the constant VIDEO_PATH to define your video destination folder
	private final static String VIDEO_PATH = "/" + File.separatorChar + "Users" + File.separatorChar + "Valentina" + File.separatorChar + "Temp" + File.separatorChar;
	private final static String VIDEO_EXTENSION = ".avi";
	private final int clientID;

	public StreamShortVideoInvokerThread(final int clientID) {
		super();
		this.clientID = clientID;

	}

	@Override
	public void run() {
		System.out.println("clientID " + clientID + " START request for streamShortVideo at time "
				+ new Timestamp((new Date()).getTime()));

		YouTubeVideoStreaming youTubeVideoStreaming = new YouTubeVideoStreamingImplService()
				.getYouTubeVideoStreamingImplPort();

		// PAY ATTENTION: when you re-build the client stub this lines of code has an
		// return type error. To fix the error you need to change the return type of the
		// method streamShortVideo in the class YouTubeStreamingVideo from byte[] to
		// javax.activation.DataHandler
		DataHandler dataHandler = youTubeVideoStreaming.streamShortVideo(clientID);

		try {
			YoutubeVideoStreamingUtils.saveVideo(dataHandler,
					new File(VIDEO_PATH + "50mb_CID_" + clientID + VIDEO_EXTENSION));
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("clientID " + clientID + " END request for streamShortVideo at time "
				+ new Timestamp((new Date()).getTime()));
	}

	public int getClientID() {
		return clientID;
	}
}
